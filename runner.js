const { default: objectParser } = require('./lib/index');
const obj = require('./examples/simpleDummy01');

(async () => {
  console.log(objectParser(obj));
})();
