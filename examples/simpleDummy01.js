module.exports = {
  functions: {
    getColour: 1,
    getSize: 1,
  },
  utils: {
    trim: {
      string: 1,
      number: 0,
    },
  },
  helpers: {
    '*': 1,
  },
};
