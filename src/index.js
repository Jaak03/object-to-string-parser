import Namespace from './classes/Namespace';

/**
 * Function takes a javascript object {} and parses it into a list of colon-separated namespaces.
 * @param {object} obj javascript object to be parsed.
 */
function parseObject(obj) {
  const n = new Namespace(obj);
  return n.getString().join(',');
}

export default parseObject;
