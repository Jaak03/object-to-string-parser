import makeObject from '../helpers/object/makeObject';

class Namespace {
  constructor(namespaceObject, namespace = 'root') {
    let obj = namespaceObject;
    if (namespace === 'root') {
      obj = { root: namespaceObject };
    }

    this.setNamespaceFields(obj, namespace);
  }

  setNamespaceFields(obj, namespace) {
    this.name = namespace;

    if (typeof obj[namespace] === 'object') {
      const keys = Object.keys(obj[namespace]);
      this.children = keys.map((key) => new Namespace(makeObject(obj[namespace], key), key));
    } else {
      this.active = (obj[namespace] === 1) || (obj[namespace] === true);
    }
  }

  getString() {
    if (this.children) {
      return this.children.map((child) => {
        const childPayload = child.getString();
        if (childPayload === '') {
          return '';
        }

        if (typeof childPayload === 'string') {
          return `${this.name}:${childPayload}`;
        }

        return childPayload
          .filter((childString) => childString !== '')
          .map((childString) => `${this.name}:${childString}`);
      });
    }

    return this.active ? this.name : '';
  }
}

export default Namespace;
