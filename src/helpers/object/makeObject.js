/**
 * Takes the key and read that value from the obj parameter. It then returns a smaller object that
 * has only one entry with that key and that key's value.
 * @param {object} obj the original object that the sub-object is to be made from.
 * @param {string} key key of the new object to be made.
 * @returns An sub-object based on teh key and the original objet.
 */
function makeObject(obj, key) {
  return { [key]: obj[key] };
}

export default makeObject;
